import logo from './logo.svg';
import './App.css';
import BasicTable from './components/Table';
import "bootstrap/dist/css/bootstrap.min.css"


function App() {
  return (
    <div >
      <h1 className='text-center mb-4'>Order Manager</h1>
      <BasicTable ></BasicTable>
    </div>
  );
}

export default App;
