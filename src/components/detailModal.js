
import { Modal, Box,Typography,Select,MenuItem, Button } from "@mui/material";
import { useEffect, useState } from "react";
import { Col,Row,Label,Input} from "reactstrap"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  

function DetailModal( {isOpen,handleCloseModal,data,updateStatus}) {
    const [display,setDisplay]= useState(isOpen)
    const [registerStatusEditForm, setRegisterStatusEditForm] = useState("");
    useEffect(()=>{
        setDisplay(isOpen)
    },[isOpen])
    return (
        <div>
            <Modal
                open={display}
                onClose= {()=>{setDisplay(!display) ;handleCloseModal()}}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Text in a modal
                    </Typography>
                    <Row className="mb-4" style={{marginLeft:"5px"}}>
                        <Col sm="4"  >
                            <Row><Label>Kích cỡ</Label></Row>
                            <Row ><Input readOnly value={data.kichCo} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Salad</Label></Row>
                            <Row ><Input  readOnly value={data.salad} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Đường kính</Label></Row>
                            <Row><Input readOnly value={data.duongKinh} style={{width:"90%"}}></Input></Row>
                        </Col>
                    </Row>
                    <Row className="mb-4" style={{marginLeft:"5px"}}>
                        <Col sm="4"  >
                            <Row><Label>Sườn nướng (miếng)</Label></Row>
                            <Row ><Input readOnly value={data.suon} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Số lượng nước uống</Label></Row>
                            <Row ><Input  readOnly value={data.soLuongNuoc} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Đơn giá</Label></Row>
                            <Row><Input readOnly value={data.thanhTien} style={{width:"90%"}}></Input></Row>
                        </Col>
                    </Row>
                    <Row className="mb-4" style={{marginLeft:"5px"}}>
                        <Col sm="4"  >
                            <Row><Label>OrderID</Label></Row>
                            <Row ><Input readOnly value={data.orderId} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Voucher ID</Label></Row>
                            <Row ><Input  readOnly value={data.idVourcher} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Loại Pizza</Label></Row>
                            <Row><Input readOnly value={data.loaiPizza} style={{width:"90%"}}></Input></Row>
                        </Col>
                    </Row>
                    <Row className="mb-4" style={{marginLeft:"5px"}}>
                        <Col sm="4"  >
                            <Row><Label>Đồ uống</Label></Row>
                            <Row ><Input readOnly value={data.idLoaiNuocUong} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Giảm giá</Label></Row>
                            <Row ><Input  readOnly value={data.giamGia} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Họ và tên</Label></Row>
                            <Row><Input readOnly value={data.hoTen} style={{width:"90%"}}></Input></Row>
                        </Col>
                    </Row>
                    <Row className="mb-4" style={{marginLeft:"5px"}}>
                        <Col sm="4"  >
                            <Row><Label>Email</Label></Row>
                            <Row ><Input readOnly value={data.email} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Số điện thoại</Label></Row>
                            <Row ><Input  readOnly value={data.soDienThoai} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Địa chỉ</Label></Row>
                            <Row><Input readOnly value={data.diaChi} style={{width:"90%"}}></Input></Row>
                        </Col>
                    </Row>
                    <Row className="mb-4" style={{marginLeft:"5px"}}>
                        <Col sm="4"  >
                            <Row><Label>Trạng thái đơn hàng</Label></Row>
                            <Row >
                                <Select  
                                    defaultValue={data.trangThai} 
                                    style={{width:"90%",height:"2.3rem"}}
                                    onChange={(event)=>{setRegisterStatusEditForm(event.target.value)}}
                                >
                                    <MenuItem value="open">Open</MenuItem>
                                    <MenuItem value="cancel">Đã hủy</MenuItem>
                                    <MenuItem value="confirmed">Đã xác nhận</MenuItem>
                                </Select>
                            </Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Ngày tạo đơn</Label></Row>
                            <Row ><Input  readOnly value={data.ngayTao} style={{width:"90%"}}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Ngày cập nhật</Label></Row>
                            <Row><Input readOnly value={data.ngayCapNhat} style={{width:"90%"}}></Input></Row>
                        </Col>
                    </Row>
                    <Row style={{marginLeft:"5px"}}>
                    <Col sm="12"  >
                            <Row><Label>Lời nhắn</Label></Row>
                            <Row ><Input readOnly  value={data.loiNhan} style={{width:"97%",minHeight:"5rem"}}></Input></Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4" >
                            <Button variant="contained" color="info" onClick={()=>{updateStatus(registerStatusEditForm,data.id)}}>Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={()=>{handleCloseModal()}}>Cancle</Button>
                        </Col>
                    </Row>
                </Box>    
            </Modal>
        </div>
    );
}
export default DetailModal