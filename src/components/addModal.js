
import { Modal, Box, Typography, Select, MenuItem, Button, Snackbar, Alert } from "@mui/material";
import { useEffect, useState } from "react";
import { Col, Row, Label, Input } from "reactstrap"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


function AddModal({ isOpen, handleCloseModal, addOrder }) {
    const Combo = [
        { kichCo: "S", duongKinh: 20, suon: 2, salad: 200, soLuongNuoc: 2, thanhTien: 150000 },
        { kichCo: "M", duongKinh: 25, suon: 4, salad: 300, soLuongNuoc: 3, thanhTien: 200000 },
        { kichCo: "L", duongKinh: 30, suon: 8, salad: 500, soLuongNuoc: 4, thanhTien: 250000 },
    ]
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }

    var vNewOrder = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    }
    const [display, setDisplay] = useState(isOpen)
    const [newOrder, setNewOrder] = useState(vNewOrder)
    const [drinks, setDrinks] = useState([])
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    useEffect(() => {
        getData("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                setDrinks(data)
            })
    }, [])
    const handleChangeCombo = (event) => {
        let chosenCombo = Combo.filter(combo => combo.kichCo === event.target.value)[0]
        setNewOrder({ ...newOrder, ...chosenCombo })

    }
    const handleChangeDrink = (event) => {
        setNewOrder({ ...newOrder, idLoaiNuocUong: event.target.value })

    }
    const handleChangeLoaiPizza = (event) => {
        setNewOrder({ ...newOrder, loaiPizza: event.target.value })
    }
    const handleChangeAddress = (event) => {
        setNewOrder({ ...newOrder, diaChi: event.target.value })

    }
    const handleChangeEmail = (event) => {
        setNewOrder({ ...newOrder, email: event.target.value })

    }
    const handleChangeMess = (event) => {
        setNewOrder({ ...newOrder, loiNhan: event.target.value })

    }
    const handleChangeName = (event) => {
        setNewOrder({ ...newOrder, hoTen: event.target.value })

    }
    const handleChangePhone = (event) => {
        setNewOrder({ ...newOrder, soDienThoai: event.target.value })

    }
    const handleChangeVoucher = (event) => {
        setNewOrder({ ...newOrder, idVourcher: event.target.value })

    }
    
    const checkValidOrder = (paramOrder) => {
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (paramOrder.kichCo === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("chưa nhập kích cỡ")
            return false
        }
        if (paramOrder.idVourcher !== "" && isNaN(paramOrder.idVourcher)) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Voucher chỉ chứa số")
            return false
        }
        if (paramOrder.idLoaiNuocUong === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("chưa chọn nước uống")
            return false
        }
        if (paramOrder.hoTen === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("chưa nhập Họ Tên")
            return false
        }
        if (paramOrder.soDienThoai === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("chưa nhập số điện thoại")
            return false
        }
        if (paramOrder.diaChi === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("chưa nhập địa chỉ")
            return false
        }
        if (paramOrder.email !== "" && !paramOrder.email.match(regexEmail)) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("email không hợp lệ")
            return false
        }
        return true
    }
    const handleConfirmBtn = () => {
        let isValid = checkValidOrder(newOrder)
        if (isValid) {
            getData("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + newOrder.idVourcher)
                .then((data) => {
                    let discount= parseInt(data.phanTramGiamGia)
                    if (discount >100 || discount <0) {discount=0 }
                    addOrder({ ...newOrder, thanhTien: newOrder.thanhTien * (100 - discount) / 100 });
                })
                .catch((error) => {
                    setOpenAlert(true);
                    setStatusModal("warning")
                    setNoidungAlertValid("voucher không chính xác")
                    addOrder({ ...newOrder })
                })
        }

    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    useEffect(() => {
        setNewOrder(vNewOrder)
        setDisplay(isOpen)
    }, [isOpen])

    return (
        <div>
            <Modal
                open={display}
                onClose={() => { setDisplay(!display); handleCloseModal() }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Text in a modal
                    </Typography>
                    <Row className="mb-4" style={{ marginLeft: "5px" }}>
                        <Col sm="4"  >
                            <Row><Label>Kích cỡ</Label></Row>
                            <Row >
                                <Select
                                    style={{ width: "90%", height: "2.3rem" }}
                                    onChange={(event) => handleChangeCombo(event)}
                                >
                                    <MenuItem value="S">Size S</MenuItem>
                                    <MenuItem value="M">Size M</MenuItem>
                                    <MenuItem value="L">Size L</MenuItem>
                                </Select>
                            </Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Salad</Label></Row>
                            <Row ><Input readOnly style={{ width: "90%" }} value={newOrder.salad}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Đường kính</Label></Row>
                            <Row><Input readOnly style={{ width: "90%" }} value={newOrder.duongKinh}></Input></Row>
                        </Col>
                    </Row>
                    <Row className="mb-4" style={{ marginLeft: "5px" }}>
                        <Col sm="4"  >
                            <Row><Label>Sườn nướng (miếng)</Label></Row>
                            <Row ><Input readOnly style={{ width: "90%" }} value={newOrder.suon}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Số lượng nước uống</Label></Row>
                            <Row ><Input readOnly style={{ width: "90%" }} value={newOrder.soLuongNuoc}></Input></Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Đơn giá</Label></Row>
                            <Row><Input readOnly style={{ width: "90%" }} value={newOrder.thanhTien}></Input></Row>
                        </Col>
                    </Row>
                    <Row className="mb-4" style={{ marginLeft: "5px" }}>
                        <Col sm="4"  >
                            <Row><Label>Loại Pizza</Label></Row>
                            <Row ><Select
                                style={{ width: "90%", height: "2.3rem" }}
                                onChange={(event) => handleChangeLoaiPizza(event)}
                            >
                                <MenuItem value="Seafood">Hải sản</MenuItem>
                                <MenuItem value="Hawaii">Hawaii</MenuItem>
                                <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                            </Select>
                            </Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Đồ uống</Label></Row>
                            <Row >
                                <Select
                                    style={{ width: "90%", height: "2.3rem" }}
                                    onChange={(event) => handleChangeDrink(event)}
                                >
                                    {drinks.map((drink, index) => (
                                        <MenuItem key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                                    ))}
                                </Select>
                            </Row>
                        </Col>
                        <Col sm="4">
                            <Row><Label>Voucher ID</Label></Row>
                            <Row><Input style={{ width: "90%" }} onChange={(event) => { handleChangeVoucher(event) }}></Input></Row>
                        </Col>
                    </Row>
                    <Row className="mb-4" style={{ marginLeft: "5px" }}>
                        <Col sm="5"  >
                            <Row><Label>Họ và tên</Label></Row>
                            <Row ><Input style={{ width: "90%" }} onChange={(event) => { handleChangeName(event) }}></Input></Row>
                        </Col>
                        <Col sm="5">
                            <Row><Label>Số điện thoại</Label></Row>
                            <Row ><Input style={{ width: "90%" }} onChange={(event) => { handleChangePhone(event) }}></Input></Row>
                        </Col>
                    </Row>
                    <Row className="mb-4" style={{ marginLeft: "5px" }}>
                        <Col sm="5"  >
                            <Row><Label>Email</Label></Row>
                            <Row ><Input style={{ width: "90%" }} onChange={(event) => { handleChangeEmail(event) }}></Input></Row>
                        </Col>
                        <Col sm="5">
                            <Row><Label>Địa chỉ</Label></Row>
                            <Row><Input style={{ width: "90%" }} onChange={(event) => { handleChangeAddress(event) }}></Input></Row>
                        </Col>
                    </Row>
                    <Row style={{ marginLeft: "5px" }}>
                        <Col sm="12"  >
                            <Row><Label>Lời nhắn</Label></Row>
                            <Row ><Input style={{ width: "97%", minHeight: "5rem" }} onChange={(event) => { handleChangeMess(event) }}></Input></Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12" className="mt-3 text-center" style={{ marginRight: '3rem' }} >
                            <Button variant="contained" color="info" onClick={() => { handleConfirmBtn() }}>Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "2rem" }} onClick={() => { handleCloseModal() }}>Cancle</Button>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
export default AddModal