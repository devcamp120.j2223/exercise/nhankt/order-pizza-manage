import { Button, Container, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Pagination, Snackbar, Alert } from "@mui/material"
import { useEffect, useState } from "react";
import AddModal from "./addModal";
import DeleteModal from "./deleteModal";
import DetailModal from "./detailModal";





export default function BasicTable() {
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [orders, setOrders] = useState([])
    const [noPage, setNoPage] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [refreshPage, setRefreshPage] = useState(Boolean(true))
    //state hiển thị các modal
    const [DetailModalDisplay, setDetailModalDisPlay] = useState(Boolean(false))
    const [DeleteModalDisplay, setDeleteModalDisPlay] = useState(Boolean(false))
    const [AddModalDisplay, setAddModalDisPlay] = useState(Boolean(false))
    //state dữ liệu khi ấn các nút của 1 order
    const [dataOfRow, setDataOfRow] = useState({})
    //state thông báo
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    //fetch
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    //sự kiện load trang, tải dữ liệu
    useEffect(() => {
        getData("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((data) => {
                setNoPage(Math.ceil(data.length / rowsPerPage))
                setOrders(data.slice((currentPage - 1) * rowsPerPage, currentPage * rowsPerPage));

            })
            .catch((error) => {
 
            })
    }, [currentPage, rowsPerPage, refreshPage])
    const changePage = (event, value) => {
        setCurrentPage(value)
    }
    const changeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value))
    }
    //bấm các nút để hiện modal
    const onDetailBtnClick = (order) => {
        setDetailModalDisPlay(!DetailModalDisplay)
        setDataOfRow(order)
    }
    const onDeleteBtnClick = (paramId) => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
        setDataOfRow(paramId)

    }
    const onBtnAddClick = ()=>{
        setAddModalDisPlay(!AddModalDisplay)
    }

    //Xử lý khi đóng các modal components
    const closeDetailModal = () => {
        setDetailModalDisPlay(!DetailModalDisplay)
    }
    const closeDeleteModal = () => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
    }
    const closeAddModal = () => {
        setAddModalDisPlay(!AddModalDisplay)
    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    //Xử lý update order
    const handleUpdateStatus = (paramStatus, paramId) => {
        let body = {
            method: 'PUT',
            body: JSON.stringify({
                trangThai: paramStatus,
            }),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
        };

        getData("http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramId, body)
            .then((data) => {
                closeDetailModal();
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã sửa order")
                setRefreshPage(!refreshPage)
            })


    }
    //Xử lý delete Order
    const handleDeleteOrder = (paramId) => {
        let body = {
            method: 'DELETE',
        };
        const deleteOrder = async (body) => {
            const response = await fetch("http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramId, body);
            closeDeleteModal();
            setOpenAlert(true);
            setStatusModal("warning")
            setNoidungAlertValid("đã xoá order")
            setRefreshPage(!refreshPage)
        }
        deleteOrder(body)
    }

    //Xử lý add Order
    const handleAddOrder = (paramOrder) => {
        const body = {
            method: 'POST',
            body: JSON.stringify(paramOrder),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
            .then((data) =>{
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã thêm order")
                closeAddModal();
                setRefreshPage(!refreshPage)
            })
            .catch((error) =>{
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Dữ liệu thêm thất bại!")
            })
        
    }

    return (
        <div>
            <Container>
                <Grid container className="mb-3"> 
                    <Grid item xs={10}>
                        <label >Show
                            <input style={{ width: "5rem", textAlign: "center" }} type={"number"} value={rowsPerPage} onChange={changeRowsPerPage}>
                            </input> entries
                        </label> 
                        </Grid>
                        <Grid item xs={2} style={{justifyContent: "flex-end"}}>
                        <Button variant="contained" color="success" onClick={onBtnAddClick} >Add Order</Button>
                        </Grid> 
                </Grid>
                <Grid>
                    <Grid item>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="right">OrderID</TableCell>
                                        <TableCell align="right">Size</TableCell>
                                        <TableCell align="right">Loại Pizza</TableCell>
                                        <TableCell align="right">Nước uống</TableCell>
                                        <TableCell align="right">Thành tiền</TableCell>
                                        <TableCell align="right">Họ và tên</TableCell>
                                        <TableCell align="right">Số điện thoại</TableCell>
                                        <TableCell align="right">Trạng Thái</TableCell>
                                        <TableCell align="center">Chi tiết</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {orders.map((order, index) => (
                                        <TableRow
                                            key={index}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell align="right">{order.orderId}</TableCell>
                                            <TableCell align="right">{order.kichCo}</TableCell>
                                            <TableCell align="right">{order.loaiPizza}</TableCell>
                                            <TableCell align="right">{order.idLoaiNuocUong}</TableCell>
                                            <TableCell align="right">{order.thanhTien}</TableCell>
                                            <TableCell align="right">{order.hoTen}</TableCell>
                                            <TableCell align="right">{order.soDienThoai}</TableCell>
                                            <TableCell align="right">{order.trangThai}</TableCell>
                                            <TableCell align="center" >
                                                <Button variant="contained" color="info" onClick={() => onDetailBtnClick(order)}>Detail</Button>
                                                <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => onDeleteBtnClick(order.id)}>Delete</Button>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>
                <Grid container mt={3} mb={2} justifyContent={"flex-end"}>
                    <Grid item>
                        <Pagination count={noPage} variant="outlined" onChange={changePage} />
                    </Grid>
                </Grid>

            </Container>
            <DetailModal isOpen={DetailModalDisplay} handleCloseModal={closeDetailModal} data={dataOfRow} updateStatus={handleUpdateStatus}></DetailModal>
            <DeleteModal isOpen={DeleteModalDisplay} handleCloseModal={closeDeleteModal} data={dataOfRow} deleteOrder={handleDeleteOrder}></DeleteModal>
            <AddModal isOpen={AddModalDisplay} handleCloseModal={closeAddModal} addOrder={handleAddOrder}></AddModal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'bottom',horizontal: 'center' }}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
